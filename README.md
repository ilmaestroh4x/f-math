# F Math (fast math)

## Overview
This library is based primarily on the idea of minimizing memory allocation.
This is achieved by exploiting the fact Java passes arrays by reference meaning the contents of said array are mutable.

### Design Reason
After creating more traditional implementations of these common data structues in Java, it became clear through profiling that on 'hot' code paths, doing a great deal of math was causing a surprising amount of memory allocations.
The problem with this is, Java cleans up this memory with a [Stop the World](https://en.wikipedia.org/wiki/Tracing_garbage_collection#Stop-the-world_vs._incremental_vs._concurrent) event.
This event can cause lag on more memory restricded devices such as phones, tablets and even desktop computers in extreme cases.

Another side benifit of this architectural decision is that depending on your project needs, it can be common place to push large quantities of data to the GPU for rendering and/or compute reasons.

### Use Case Example
A 3D model has a set of vertices. Each of these are typically 3 float values representing the 'X','Y','Z' coordinates of that vertex in model space.

It can be required that some operation may need to be executed on these verticies in some way (for instance calculating surface normals).

In Java, developers don't have access to the underlying memory meaning they are stuck feeding this data as a float array.
This can be annoying because if you use a math library that treats each vertex/normal/etc as a 'Vector' object, to push that data to the GPU you'd first need to loop through all of these objects, and copy their values to one float array.

This library tries to help with that problem by treating the data as raw float arrays from the start to eliminate the need to do any unneeded work(allocating or processing data).

Here is a quick contrived example. (Though a more real-world example could be the updating of matricies used for skeletal animation allowing all matricies to be pushed quickly by a single OpenGL command.)
```java
    int stride = 3;
    float[] vectors_to_be_normalized = ...
    int count = vectors_to_be_normalized.length / stride;
    
    for (int offset = 0; offset < vectors_to_be_normalized.length; offset += stride) {
        Vec3.norm( // normalize
        
            vectors_to_be_normalized, // the array to store the resulting values (in this case it's mutating the original array)
            offset, // offset in that array to start writing the resulting values
            
            vectors_to_be_normalized, // the input array
            offset // the offset from which to start reading the values
            
        );
    }
    
    glUniform3fv(SOME_LOCATION, count, vectors_to_be_normalized);
```

If the above used a more traditional 'Vector3' object instead, it may look something like the following:
```java
    int stride = 3;
    Vector3[] vectors_to_be_normalized = ...
    int count = vectors_to_be_normalized.length;
    
    // now we need an additional storage to hold the data in a format to be sent to the GPU
    float[] normalize_vectors_as_array = new float[vectors_to_be_normalized.length * stride];
    
    for (int i = 0; i < vectors_to_be_normalized.length; i++) {
        vectors_to_be_normalized[i].norm(); // normalize
        
        // extra overhead from copying the vectors data
        System.arraycopy(
            vectors_to_be_normalized[i].data,   // source to be copied
            0,                                  // start position of source
            normalize_vectors_as_array,         // destination to be copied into
            i * stride,                         // start position of destination
            stride                              // length of copy
        );
    }
    
    glUniform3fv(SOME_LOCATION, count, normalize_vectors_as_array);
```

## License
See 'LICENSE' in project root.