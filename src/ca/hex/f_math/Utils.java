package ca.hex.f_math;

import static java.lang.Math.PI;

public final class Utils {
    private Utils() {}

    public static final float TO_DEGREES = (float) (180 / PI);
    public static final float TO_RADIANS = (float) (PI / 180);

    public static float clamp01(
            float value
    ) {
        return clamp(value, 0, 1);
    }

    public static float clamp(
            float value,
            float min,
            float max
    ) {
        return Math.max(min, Math.min(value, max));
    }
}
