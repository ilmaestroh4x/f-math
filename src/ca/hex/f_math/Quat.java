package ca.hex.f_math;

import static ca.hex.f_math.Utils.TO_DEGREES;
import static ca.hex.f_math.Utils.TO_RADIANS;
import static java.lang.Math.*;
import static java.lang.System.arraycopy;

public final class Quat {
    private Quat() { }

    /**
     * quaternion format [X, Y, Z, W]
     */

    private static final float HALF_PI = (float) (PI / 2.0);
    private static final float TWO_PI  = (float) (PI * 2.0);

    private static final float[] VEC_RT = {1, 0, 0};
    private static final float[] VEC_UP = {0, 1, 0};
    private static final float[] VEC_FW = {0, 0, 1};

    private final static float[] temp = new float[9];

    private static int rhs_X = 0, lhs_X = 0, q_X = 0, res_X = 0;
    private static int rhs_Y = 0, lhs_Y = 0, q_Y = 0, res_Y = 0;
    private static int rhs_Z = 0, lhs_Z = 0, q_Z = 0, res_Z = 0;
    private static int rhs_W = 0, lhs_W = 0, q_W = 0, res_W = 0;

    public static boolean equals(
            float[] lhs,
            float[] rhs
    ) {
        return
            lhs[0] == rhs[0] &&
            lhs[1] == rhs[1] &&
            lhs[2] == rhs[2] &&
            lhs[3] == rhs[3];
    }

    public static boolean equals(
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        return
            lhs[lhsOffset    ] == rhs[rhsOffset    ] &&
            lhs[lhsOffset + 1] == rhs[rhsOffset + 1] &&
            lhs[lhsOffset + 2] == rhs[rhsOffset + 2] &&
            lhs[lhsOffset + 3] == rhs[rhsOffset + 3];
    }

    public static void copy(
            float[] result,
            float[] lhs
    ) {
        arraycopy(lhs, 0, result, 0, 4);
    }

    public static void copy(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset
    ) {
        arraycopy(lhs, lhsOffset, result, resultOffset, 4);
    }

    public static float[] identity(
            float[] result
    ) {
        result[0] = 0;
        result[1] = 0;
        result[2] = 0;
        result[3] = 1;

        return result;
    }

    public static void identity(
            float[] result, int resultOffset
    ) {
        result[resultOffset    ] = 0;
        result[resultOffset + 1] = 0;
        result[resultOffset + 2] = 0;
        result[resultOffset + 3] = 1;
    }

    public static float[] localForward(
            float[] result,
            float[] q
    ) {
        mulV(result, q, VEC_FW);

        return result;
    }

    public static void localForward(
            float[] result, int resultOffset,
            float[] q,      int qOffset
    ) {
        mulV(result, resultOffset, q, qOffset, VEC_FW, 0);
    }

    public static float[] localUp(
            float[] result,
            float[] q
    ) {
        mulV(result, q, VEC_UP);

        return result;
    }

    public static void localUp(
            float[] result, int resultOffset,
            float[] q,      int qOffset
    ) {
        mulV(result, resultOffset, q, qOffset, VEC_UP, 0);
    }

    public static float[] localRight(
            float[] result,
            float[] q
    ) {
        mulV(result, q, VEC_RT);

        return result;
    }

    public static void localRight(
            float[] result, int resultOffset,
            float[] q,      int qOffset
    ) {
        mulV(result, resultOffset, q, qOffset, VEC_RT, 0);
    }

    public static float[] add(
            float[] result,
            float[] lhs,
            float[] rhs
    ) {
        result[0] = lhs[0] + rhs[0];
        result[1] = lhs[1] + rhs[1];
        result[2] = lhs[2] + rhs[2];
        result[3] = lhs[3] + rhs[3];

        return result;
    }

    public static void add(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        result[resultOffset    ] = lhs[lhsOffset    ] + rhs[rhsOffset    ];
        result[resultOffset + 1] = lhs[lhsOffset + 1] + rhs[rhsOffset + 1];
        result[resultOffset + 2] = lhs[lhsOffset + 2] + rhs[rhsOffset + 2];
        result[resultOffset + 3] = lhs[lhsOffset + 3] + rhs[rhsOffset + 3];
    }

    public static float[] sub(
            float[] result,
            float[] lhs,
            float[] rhs
    ) {
        result[0] = lhs[0] - rhs[0];
        result[1] = lhs[1] - rhs[1];
        result[2] = lhs[2] - rhs[2];
        result[3] = lhs[3] - rhs[3];

        return result;
    }

    public static void sub(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        result[resultOffset    ] = lhs[lhsOffset    ] - rhs[rhsOffset    ];
        result[resultOffset + 1] = lhs[lhsOffset + 1] - rhs[rhsOffset + 1];
        result[resultOffset + 2] = lhs[lhsOffset + 2] - rhs[rhsOffset + 2];
        result[resultOffset + 3] = lhs[lhsOffset + 3] - rhs[rhsOffset + 3];
    }

    public static float[] mul(
            float[] result,
            float[] q,
            float scalar
    ) {
        result[0] = q[0] * scalar;
        result[1] = q[1] * scalar;
        result[2] = q[2] * scalar;
        result[3] = q[3] * scalar;

        return result;
    }

    public static void mul(
            float[] result, int resultOffset,
            float[] q,      int qOffset,
            float scalar
    ) {
        result[resultOffset    ] = q[qOffset    ] * scalar;
        result[resultOffset + 1] = q[qOffset + 1] * scalar;
        result[resultOffset + 2] = q[qOffset + 2] * scalar;
        result[resultOffset + 3] = q[qOffset + 3] * scalar;
    }

    public static float[] mulQ(
            float[] result,
            float[] lhs,
            float[] rhs
    ) {
        temp[0] = rhs[3] * lhs[0] + rhs[0] * lhs[3] - rhs[1] * lhs[2] + rhs[2] * lhs[1];
        temp[1] = rhs[3] * lhs[1] + rhs[0] * lhs[2] + rhs[1] * lhs[3] - rhs[2] * lhs[0];
        temp[2] = rhs[3] * lhs[2] - rhs[0] * lhs[1] + rhs[1] * lhs[0] + rhs[2] * lhs[3];
        temp[3] = rhs[3] * lhs[3] - rhs[0] * lhs[0] - rhs[1] * lhs[1] - rhs[2] * lhs[2];

        result[0] = temp[0];
        result[1] = temp[1];
        result[2] = temp[2];
        result[3] = temp[3];

        return result;
    }

    public static void mulQ(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        lhs_X = lhsOffset;
        lhs_Y = lhsOffset + 1;
        lhs_Z = lhsOffset + 2;
        lhs_W = lhsOffset + 3;

        rhs_X = rhsOffset;
        rhs_Y = rhsOffset + 1;
        rhs_Z = rhsOffset + 2;
        rhs_W = rhsOffset + 3;

        temp[0] = rhs[rhs_W] * lhs[lhs_X] + rhs[rhs_X] * lhs[lhs_W] - rhs[rhs_Y] * lhs[lhs_Z] + rhs[rhs_Z] * lhs[lhs_Y];
        temp[1] = rhs[rhs_W] * lhs[lhs_Y] + rhs[rhs_X] * lhs[lhs_Z] + rhs[rhs_Y] * lhs[lhs_W] - rhs[rhs_Z] * lhs[lhs_X];
        temp[2] = rhs[rhs_W] * lhs[lhs_Z] - rhs[rhs_X] * lhs[lhs_Y] + rhs[rhs_Y] * lhs[lhs_X] + rhs[rhs_Z] * lhs[lhs_W];
        temp[3] = rhs[rhs_W] * lhs[lhs_W] - rhs[rhs_X] * lhs[lhs_X] - rhs[rhs_Y] * lhs[lhs_Y] - rhs[rhs_Z] * lhs[lhs_Z];

        result[resultOffset    ] = temp[0];
        result[resultOffset + 1] = temp[1];
        result[resultOffset + 2] = temp[2];
        result[resultOffset + 3] = temp[3];
    }

    public static float[] mulV(
            float[] result,
            float[] q,
            float[] v
    ) {
        /*
            t = 2 * cross(q.xyz, v)
            v' = v + q.w * t + cross(q.xyz, t)
         */

        //t = temp[0] = cross(q.xyz, v)
        Vec3.cross(temp, 0, q, 0, v, 0);
        //t = temp[0] = t * 2
        Vec3.mul(temp, 0, temp, 0, 2);

        //v' = temp[3] = t * q.w
        Vec3.mul(temp, 3, temp, 0, q[3]);
        //v' = temp[3] = v' + v
        Vec3.add(temp, 3, temp, 3, v, 0);
        //? = temp[6] = cross(q.xyz, t)
        Vec3.cross(temp, 6, q, 0, temp, 0);
        //v' = v' + ?
        Vec3.add(temp, 0, temp, 3, temp, 6);

        Vec3.norm(temp, 0, temp, 0);

        result[0] = temp[0];
        result[1] = temp[1];
        result[2] = temp[2];

        return result;
    }

    public static void mulV(
            float[] result, int resultOffset,
            float[] q,      int qOffset,
            float[] v,      int vOffset
    ) {
        /*
            t = 2 * cross(q.xyz, v)
            v' = v + q.w * t + cross(q.xyz, t)
         */

        //t = temp[0] = cross(q.xyz, v)
        Vec3.cross(temp, 0, q, qOffset, v, vOffset);
        //t = temp[0] = t * 2
        Vec3.mul(temp, 0, temp, 0, 2);

        //v' = temp[3] = t * q.w
        Vec3.mul(temp, 3, temp, 0, q[qOffset + 3]);
        //v' = temp[3] = v' + v
        Vec3.add(temp, 3, temp, 3, v, vOffset);
        //? = temp[6] = cross(q.xyz, t)
        Vec3.cross(temp, 6, q, qOffset, temp, 0);
        //v' = v' + ?
        Vec3.add(temp, 0, temp, 3, temp, 6);

        Vec3.norm(temp, 0, temp, 0);

        result[resultOffset    ] = temp[0];
        result[resultOffset + 1] = temp[1];
        result[resultOffset + 2] = temp[2];
    }

    public static float[] div(
            float[] result,
            float[] q,
            float scalar
    ) {
        result[0] = q[0] / scalar;
        result[1] = q[1] / scalar;
        result[2] = q[2] / scalar;
        result[3] = q[3] / scalar;

        return result;
    }

    public static void div(
            float[] result, int resultOffset,
            float[] q,      int qOffset,
            float scalar
    ) {
        result[resultOffset    ] = q[qOffset    ] / scalar;
        result[resultOffset + 1] = q[qOffset + 1] / scalar;
        result[resultOffset + 2] = q[qOffset + 2] / scalar;
        result[resultOffset + 3] = q[qOffset + 3] / scalar;
    }

    public static float dot(
            float[] result,
            float[] lhs,
            float[] rhs
    ) {
        temp[0] = lhs[0] * rhs[0];
        temp[1] = lhs[1] * rhs[1];
        temp[2] = lhs[2] * rhs[2];
        temp[3] = lhs[3] * rhs[3];

        return result[0] = temp[0] + temp[1] + temp[2] + temp[3];
    }

    public static void dot(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        temp[0] = lhs[lhsOffset    ] * rhs[rhsOffset    ];
        temp[1] = lhs[lhsOffset + 1] * rhs[rhsOffset + 1];
        temp[2] = lhs[lhsOffset + 2] * rhs[rhsOffset + 2];
        temp[3] = lhs[lhsOffset + 3] * rhs[rhsOffset + 3];

        result[resultOffset] = temp[0] + temp[1] + temp[2] + temp[3];
    }

    public static float[] norm(
            float[] result,
            float[] q
    ) {
        temp[0] = q[0] * q[0];
        temp[1] = q[1] * q[1];
        temp[2] = q[2] * q[2];
        temp[3] = q[3] * q[3];
        temp[4] = (float) sqrt(temp[0] + temp[1] + temp[2] + temp[3]);

        result[0] = q[q_X] / temp[4];
        result[1] = q[q_Y] / temp[4];
        result[2] = q[q_Z] / temp[4];
        result[3] = q[q_W] / temp[4];

        return result;
    }

    public static void norm(
            float[] result, int resultOffset,
            float[] q,      int qOffset
    ) {
        q_X = qOffset;
        q_Y = qOffset + 1;
        q_Z = qOffset + 2;
        q_W = qOffset + 3;

        temp[0] = q[q_X] * q[q_X];
        temp[1] = q[q_Y] * q[q_Y];
        temp[2] = q[q_Z] * q[q_Z];
        temp[3] = q[q_W] * q[q_W];
        temp[4] = (float) sqrt(temp[0] + temp[1] + temp[2] + temp[3]);

        result[resultOffset    ] = q[q_X] / temp[4];
        result[resultOffset + 1] = q[q_Y] / temp[4];
        result[resultOffset + 2] = q[q_Z] / temp[4];
        result[resultOffset + 3] = q[q_W] / temp[4];
    }

    public static float mag(
            float[] result,
            float[] q
    ) {
        temp[0] = q[0] * q[0];
        temp[1] = q[1] * q[1];
        temp[2] = q[2] * q[2];
        temp[3] = q[3] * q[3];

        return result[0] = (float) sqrt(temp[0] + temp[1] + temp[2] + temp[3]);
    }

    public static void mag(
            float[] result, int resultOffset,
            float[] q,      int qOffset
    ) {
        temp[0] = q[qOffset    ];
        temp[1] = q[qOffset + 1];
        temp[2] = q[qOffset + 2];
        temp[3] = q[qOffset + 3];
        temp[0] *= temp[0];
        temp[1] *= temp[1];
        temp[2] *= temp[2];
        temp[3] *= temp[3];

        result[resultOffset] = (float) sqrt(temp[0] + temp[1] + temp[2] + temp[3]);
    }

    public static float[] conj(
            float[] result,
            float[] q
    ) {
        result[0] = -q[0];
        result[1] = -q[1];
        result[2] = -q[2];
        result[3] =  q[3];

        return result;
    }

    public static void conj(
            float[] result, int resultOffset,
            float[] q,      int qOffset
    ) {
        result[resultOffset    ] = -q[qOffset    ];
        result[resultOffset + 1] = -q[qOffset + 1];
        result[resultOffset + 2] = -q[qOffset + 2];
        result[resultOffset + 3] =  q[qOffset + 3];
    }

    public static float[] invert(
            float[] result,
            float[] q
    ) {
        temp[0] = q[0] * q[0];
        temp[1] = q[1] * q[1];
        temp[2] = q[2] * q[2];
        temp[3] = q[3] * q[3];
        temp[4] = temp[0] + temp[1] + temp[2] + temp[3];

        result[0] = q[q_X] / -temp[4];
        result[1] = q[q_Y] / -temp[4];
        result[2] = q[q_Z] / -temp[4];
        result[3] = q[q_W] /  temp[4];

        return result;
    }

    public static void invert(
            float[] result, int resultOffset,
            float[] q,      int qOffset
    ) {
        q_X = qOffset;
        q_Y = qOffset + 1;
        q_Z = qOffset + 2;
        q_W = qOffset + 3;

        temp[0] = q[q_X] * q[q_X];
        temp[1] = q[q_Y] * q[q_Y];
        temp[2] = q[q_Z] * q[q_Z];
        temp[3] = q[q_W] * q[q_W];
        temp[4] = temp[0] + temp[1] + temp[2] + temp[3];

        result[resultOffset    ] = q[q_X] / -temp[4];
        result[resultOffset + 1] = q[q_Y] / -temp[4];
        result[resultOffset + 2] = q[q_Z] / -temp[4];
        result[resultOffset + 3] = q[q_W] /  temp[4];
    }

    public static float[] slerp(
            float[] result,
            float[] lhs,
            float[] rhs,
            float t,
            boolean allowFlip
    ) {
        temp[0] = lhs[0] * rhs[0];
        temp[1] = lhs[1] * rhs[1];
        temp[2] = lhs[2] * rhs[2];
        temp[3] = lhs[3] * rhs[3];

        temp[0] = temp[0] + temp[1] + temp[2] + temp[3];
        temp[1] = abs(temp[0]);

        // Linear interpolation for close orientations
        if (1 - temp[1] < 0.01f) {
            temp[2] = 1 - t;
            temp[3] = t;
        } else {
            // Spherical interpolation
            temp[1] = (float) acos(temp[1]);
            temp[4] = (float) sin(temp[1]);
            temp[2] = (float) sin(temp[1] * (1 - t)) / temp[4];
            temp[3] = (float) sin(temp[1] * t) / temp[4];
        }

        // Use the shortest path
        if (allowFlip && temp[0] < 0) {
            temp[2] = -temp[2];
        }

        result[0] = temp[2] * lhs[0] + temp[3] * rhs[0];
        result[1] = temp[2] * lhs[1] + temp[3] * rhs[1];
        result[2] = temp[2] * lhs[2] + temp[3] * rhs[2];
        result[3] = temp[2] * lhs[3] + temp[3] * rhs[3];

        return result;
    }

    public static void slerp(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset,
            float t,
            boolean allowFlip
    ) {
        lhs_X = lhsOffset;
        lhs_Y = lhsOffset + 1;
        lhs_Z = lhsOffset + 2;
        lhs_W = lhsOffset + 3;

        rhs_X = rhsOffset;
        rhs_Y = rhsOffset + 1;
        rhs_Z = rhsOffset + 2;
        rhs_W = rhsOffset + 3;

        temp[0] = lhs[lhs_X] * rhs[rhs_X];
        temp[1] = lhs[lhs_Y] * rhs[rhs_Y];
        temp[2] = lhs[lhs_Z] * rhs[rhs_Z];
        temp[3] = lhs[lhs_W] * rhs[rhs_W];

        temp[0] = temp[0] + temp[1] + temp[2] + temp[3];
        temp[1] = abs(temp[0]);

        // Linear interpolation for close orientations
        if (1 - temp[1] < 0.01f) {
            temp[2] = 1 - t;
            temp[3] = t;
        } else {
            // Spherical interpolation
            temp[1] = (float) acos(temp[1]);
            temp[4] = (float) sin(temp[1]);
            temp[2] = (float) sin(temp[1] * (1 - t)) / temp[4];
            temp[3] = (float) sin(temp[1] * t) / temp[4];
        }

        // Use the shortest path
        if (allowFlip && temp[0] < 0) {
            temp[2] = -temp[2];
        }

        result[resultOffset    ] = temp[2] * lhs[lhs_X] + temp[3] * rhs[rhs_X];
        result[resultOffset + 1] = temp[2] * lhs[lhs_Y] + temp[3] * rhs[rhs_Y];
        result[resultOffset + 2] = temp[2] * lhs[lhs_Z] + temp[3] * rhs[rhs_Z];
        result[resultOffset + 3] = temp[2] * lhs[lhs_W] + temp[3] * rhs[rhs_W];
    }

    //result = [X, Y, Z, ANGLE]
    public static float[] toAxisAngle(
            float[] result,
            float[] q,
            boolean asDegrees
    ) {
        temp[0] = q[0] * q[0];
        temp[1] = q[1] * q[1];
        temp[2] = q[2] * q[2];
        temp[3] = (float) sqrt(temp[0] + temp[1] + temp[2]);

        if (temp[3] > 0) {
            result[0] = q[0] / temp[3];
            result[1] = q[1] / temp[3];
            result[2] = q[2] / temp[3];
        } else {
            result[0] = 1;
            result[1] = 0;
            result[2] = 0;
        }

        temp[3] = (float) acos(q[q_W]);
        if (temp[3] > HALF_PI) {
            result[0] = -result[0];
            result[1] = -result[1];
            result[2] = -result[2];
        }

        temp[3] *= 2;
        result[3] = temp[3] <= PI ? temp[3] : TWO_PI - temp[3];
        if (asDegrees) {
            result[3] *= TO_DEGREES;
        }

        return result;
    }

    //result = [X, Y, Z, ANGLE]
    public static void toAxisAngle(
            float[] result, int resultOffset,
            float[] q,      int qOffset,
            boolean asDegrees
    ) {
        q_X = qOffset;
        q_Y = qOffset + 1;
        q_Z = qOffset + 2;
        q_W = qOffset + 3;

        res_X = resultOffset;
        res_Y = resultOffset + 1;
        res_Z = resultOffset + 2;
        res_W = resultOffset + 3;

        temp[0] = q[q_X] * q[q_X];
        temp[1] = q[q_Y] * q[q_Y];
        temp[2] = q[q_Z] * q[q_Z];
        temp[3] = (float) sqrt(temp[0] + temp[1] + temp[2]);

        if (temp[3] > 0) {
            result[res_X] = q[q_X] / temp[3];
            result[res_Y] = q[q_Y] / temp[3];
            result[res_Z] = q[q_Z] / temp[3];
        } else {
            result[res_X] = 1;
            result[res_Y] = 0;
            result[res_Z] = 0;
        }

        temp[3] = (float) acos(q[q_W]);
        if (temp[3] > HALF_PI) {
            result[res_X] = -result[res_X];
            result[res_Y] = -result[res_Y];
            result[res_Z] = -result[res_Z];
        }

        temp[3] *= 2;
        result[res_W] = temp[3] <= PI ? temp[3] : TWO_PI - temp[3];
        if (asDegrees) {
            result[res_W] *= TO_DEGREES;
        }
    }

    public static float[] fromAxisAngle(
            float[] result,
            float[] v,
            boolean inDegrees
    ) {
        temp[0] = v[0] * v[0];
        temp[1] = v[1] * v[1];
        temp[2] = v[2] * v[2];
        temp[3] = (float) sqrt(temp[0] + temp[1] + temp[2]);

        if (temp[3] > 0) {
            temp[3] = v[3] / 2;
            if (inDegrees) {
                temp[3] *= TO_RADIANS;
            }
            if (temp[3] == 0) {
                //prevent angle from being exactly zero.
                temp[3] = 1.e-6f;
            }
            temp[4] = (float) sin(temp[3]);
            result[0] = v[0] * temp[4];
            result[1] = v[1] * temp[4];
            result[2] = v[2] * temp[4];
            result[3] = (float) cos(temp[3]);
        } else {
            result[0] = 0;
            result[1] = 0;
            result[2] = 0;
            result[3] = 1;
        }

        return result;
    }

    public static void fromAxisAngle(
            float[] result, int resultOffset,
            float[] v,      int vOffset,
            boolean inDegrees
    ) {
        q_X = vOffset;
        q_Y = vOffset + 1;
        q_Z = vOffset + 2;
        q_W = vOffset + 3;

        temp[0] = v[q_X] * v[q_X];
        temp[1] = v[q_Y] * v[q_Y];
        temp[2] = v[q_Z] * v[q_Z];
        temp[3] = (float) sqrt(temp[0] + temp[1] + temp[2]);

        if (temp[3] > 0) {
            temp[3] = v[q_W] / 2;
            if (inDegrees) {
                temp[3] *= TO_RADIANS;
            }
            if (temp[3] == 0) {
                //prevent angle from being exactly zero.
                temp[3] = 1.e-6f;
            }
            temp[4] = (float) sin(temp[3]);
            result[resultOffset    ] = v[q_X] * temp[4];
            result[resultOffset + 1] = v[q_Y] * temp[4];
            result[resultOffset + 2] = v[q_Z] * temp[4];
            result[resultOffset + 3] = (float) cos(temp[3]);
        } else {
            result[resultOffset    ] = 0;
            result[resultOffset + 1] = 0;
            result[resultOffset + 2] = 0;
            result[resultOffset + 3] = 1;
        }
    }

    //TODO: FIX, NOT RETURNING EXPECTED ANGLES!

    public static float[] toEulerAngles(
            float[] result,
            float[] q,
            boolean asDegrees
    ) {
        temp[3] = q[0] * q[1] + q[2] * q[3];
        if (temp[3] > 0.499) {
            temp[0] = 0;
            temp[1] = 2 * (float) atan2(q[0], q[3]);
            temp[2] = HALF_PI;
        } else
        if (temp[3] < -0.499) {
            temp[0] = 0;
            temp[1] = -2 * (float) atan2(q[0], q[3]);
            temp[2] = -HALF_PI;
        } else {
            temp[4] = q[2] * q[2] * 2;
            temp[0] = (float) atan2(2 * q[0] * q[3] - 2 * q[1] * q[2], 1 - 2 * q[0] * q[0] - temp[4]);
            temp[1] = (float) atan2(2 * q[1] * q[3] - 2 * q[0] * q[2], 1 - 2 * q[1] * q[1] - temp[4]);
            temp[2] = (float) asin(2 * temp[3]);
        }

        if (asDegrees) {
            temp[0] *= TO_DEGREES;
            temp[1] *= TO_DEGREES;
            temp[2] *= TO_DEGREES;
        }

        result[0] = temp[0];
        result[1] = temp[1];
        result[2] = temp[2];

        return result;
    }

    public static void toEulerAngles(
            float[] result, int resultOffset,
            float[] q,      int qOffset,
            boolean asDegrees
    ) {
        q_X = qOffset;
        q_Y = qOffset + 1;
        q_Z = qOffset + 2;
        q_W = qOffset + 3;

        temp[3] = q[q_X] * q[q_Y] + q[q_Z] * q[q_W];
        if (temp[3] > 0.499) {
            temp[0] = 0;
            temp[1] = 2 * (float) atan2(q[q_X], q[q_W]);
            temp[2] = HALF_PI;
        } else
        if (temp[3] < -0.499) {
            temp[0] = 0;
            temp[1] = -2 * (float) atan2(q[q_X], q[q_W]);
            temp[2] = -HALF_PI;
        } else {
            temp[4] = q[q_Z] * q[q_Z] * 2;
            temp[0] = (float) atan2(2 * q[q_X] * q[q_W] - 2 * q[q_Y] * q[q_Z], 1 - 2 * q[q_X] * q[q_X] - temp[4]);
            temp[1] = (float) atan2(2 * q[q_Y] * q[q_W] - 2 * q[q_X] * q[q_Z], 1 - 2 * q[q_Y] * q[q_Y] - temp[4]);
            temp[2] = (float) asin(2 * temp[3]);
        }

        if (asDegrees) {
            temp[0] *= TO_DEGREES;
            temp[1] *= TO_DEGREES;
            temp[2] *= TO_DEGREES;
        }

        result[resultOffset    ] = temp[0];
        result[resultOffset + 1] = temp[1];
        result[resultOffset + 2] = temp[2];

//http://bediyap.com/programming/convert-quaternion-to-euler-rotations/
/*
        // roll (x-axis rotation)
        double sinr =       2.0 * (q[q_W] * q[q_X] + q[q_Y] * q[q_Z]);
        double cosr = 1.0 - 2.0 * (q[q_X] * q[q_X] + q[q_Y] * q[q_Y]);
        result[resultOffset    ] = (float) atan2(sinr, cosr);

        // pitch (y-axis rotation)
        double sinp = 2.0 * (q[q_W] * q[q_Y] - q[q_Z] * q[q_X]);
        if (abs(sinp) >= 1) {
            result[resultOffset + 1] = (float) copySign(HALF_PI, sinp);
        } else {
            result[resultOffset + 1] = (float) asin(sinp);
        }

        // yaw (z-axis rotation)
        double siny =       2.0 * (q[q_W] * q[q_Z] + q[q_X] * q[q_Y]);
        double cosy = 1.0 - 2.0 * (q[q_Y] * q[q_Y] + q[q_Z] * q[q_Z]);
        result[resultOffset + 2] = (float) atan2(siny, cosy);

        result[resultOffset    ] *= TO_DEGREES;
        result[resultOffset + 1] *= TO_DEGREES;
        result[resultOffset + 2] *= TO_DEGREES;
//*/
/*
        result[resultOffset    ] = (float) atan2(2 * q[q_Y] * q[q_W] + 2 * q[q_X] * q[q_Z], 1 - 2 * q[q_Y] * q[q_Y] - 2 * q[q_Z] * q[q_Z]);
        result[resultOffset + 1] = (float) atan2(2 * q[q_X] * q[q_W] + 2 * q[q_Y] * q[q_Z], 1 - 2 * q[q_X] * q[q_X] - 2 * q[q_Z] * q[q_Z]);
        result[resultOffset + 2] = (float)  asin(2 * q[q_X] * q[q_Y] + 2 * q[q_Z] * q[q_W]);
//*/

    }

    public static float[] fromEulerAngles(
            float[] result,
            float[] v,
            boolean inDegrees
    ) {
        temp[0] = v[0] / 2;
        temp[1] = v[1] / 2;
        temp[2] = v[2] / 2;

        if (inDegrees) {
            temp[0] *= TO_RADIANS;
            temp[1] *= TO_RADIANS;
            temp[2] *= TO_RADIANS;
        }

        temp[3] = (float) cos(temp[0]);
        temp[4] = (float) cos(temp[1]);
        temp[5] = (float) cos(temp[2]);

        temp[6] = (float) sin(temp[0]);
        temp[7] = (float) sin(temp[1]);
        temp[8] = (float) sin(temp[2]);

        result[0] = temp[6] * temp[4] * temp[5] + temp[3] * temp[7] * temp[8];
        result[1] = temp[3] * temp[7] * temp[5] - temp[6] * temp[4] * temp[8];
        result[2] = temp[3] * temp[4] * temp[8] + temp[6] * temp[7] * temp[5];
        result[3] = temp[3] * temp[4] * temp[5] - temp[6] * temp[7] * temp[8];

        return result;
    }

    public static void fromEulerAngles(
            float[] result, int resultOffset,
            float[] v,      int vOffset,
            boolean inDegrees
    ) {
        temp[0] = v[vOffset    ] / 2;
        temp[1] = v[vOffset + 1] / 2;
        temp[2] = v[vOffset + 2] / 2;

        if (inDegrees) {
            temp[0] *= TO_RADIANS;
            temp[1] *= TO_RADIANS;
            temp[2] *= TO_RADIANS;
        }

        temp[3] = (float) cos(temp[0]);
        temp[4] = (float) cos(temp[1]);
        temp[5] = (float) cos(temp[2]);

        temp[6] = (float) sin(temp[0]);
        temp[7] = (float) sin(temp[1]);
        temp[8] = (float) sin(temp[2]);
        
        result[resultOffset    ] = temp[6] * temp[4] * temp[5] + temp[3] * temp[7] * temp[8];
        result[resultOffset + 1] = temp[3] * temp[7] * temp[5] - temp[6] * temp[4] * temp[8];
        result[resultOffset + 2] = temp[3] * temp[4] * temp[8] + temp[6] * temp[7] * temp[5];
        result[resultOffset + 3] = temp[3] * temp[4] * temp[5] - temp[6] * temp[7] * temp[8];
    }
}