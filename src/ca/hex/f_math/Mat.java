package ca.hex.f_math;

import static ca.hex.f_math.Utils.TO_RADIANS;
import static java.lang.Math.*;
import static java.lang.System.arraycopy;

//ported from http://androidxref.com/source/xref/frameworks/base/core/jni/android/opengl/util.cpp
public final class Mat {
    private Mat() { }

    private static final float[] TMP0 = new float[4];

    private final static float[] temp0 = new float[16];
    private final static float[] temp1 = new float[16];

    public static boolean equals(
            float[] lhs,
            float[] rhs
    ) {
        return
            lhs[ 0] == rhs[ 0] &&
            lhs[ 1] == rhs[ 1] &&
            lhs[ 2] == rhs[ 2] &&
            lhs[ 3] == rhs[ 3] &&
            lhs[ 4] == rhs[ 4] &&
            lhs[ 5] == rhs[ 5] &&
            lhs[ 6] == rhs[ 6] &&
            lhs[ 7] == rhs[ 7] &&
            lhs[ 8] == rhs[ 8] &&
            lhs[ 9] == rhs[ 9] &&
            lhs[10] == rhs[10] &&
            lhs[11] == rhs[11] &&
            lhs[12] == rhs[12] &&
            lhs[13] == rhs[13] &&
            lhs[14] == rhs[14] &&
            lhs[15] == rhs[15];
    }

    public static void multiplyMM(float[] result_mat4, float[] lhs_mat4, float[] rhs_mat4) {
        for (int i = 0; i < 4; i++) {
            int i4 = 4 * i;
            float rhs_i0 = rhs_mat4[i4];
            float ri0 = lhs_mat4[0] * rhs_i0;
            float ri1 = lhs_mat4[1] * rhs_i0;
            float ri2 = lhs_mat4[2] * rhs_i0;
            float ri3 = lhs_mat4[3] * rhs_i0;

            for (int j = 1; j < 4; j++) {
                int j4 = 4 * j;
                float rhs_ij = rhs_mat4[i4 + j];
                ri0 += lhs_mat4[j4    ] * rhs_ij;
                ri1 += lhs_mat4[j4 + 1] * rhs_ij;
                ri2 += lhs_mat4[j4 + 2] * rhs_ij;
                ri3 += lhs_mat4[j4 + 3] * rhs_ij;
            }

            result_mat4[i4    ] = ri0;
            result_mat4[i4 + 1] = ri1;
            result_mat4[i4 + 2] = ri2;
            result_mat4[i4 + 3] = ri3;
        }
    }

    public static void multiplyMV(float[] result_vec4, float[] lhs_mat4, float[] lhs_vec4) {
        arraycopy(lhs_vec4, 0, temp0, 0, 4);
        result_vec4[0] = lhs_mat4[0] * temp0[0] + lhs_mat4[4] * temp0[1] + lhs_mat4[ 8] * temp0[2] + lhs_mat4[12] * temp0[3];
        result_vec4[1] = lhs_mat4[1] * temp0[0] + lhs_mat4[5] * temp0[1] + lhs_mat4[ 9] * temp0[2] + lhs_mat4[13] * temp0[3];
        result_vec4[2] = lhs_mat4[2] * temp0[0] + lhs_mat4[6] * temp0[1] + lhs_mat4[10] * temp0[2] + lhs_mat4[14] * temp0[3];
        result_vec4[3] = lhs_mat4[3] * temp0[0] + lhs_mat4[7] * temp0[1] + lhs_mat4[11] * temp0[2] + lhs_mat4[15] * temp0[3];
    }

    public static boolean invertM(float[] result_mat4, float[] mat4) {
        // Invert a 4 x 4 matrix using Cramer's Rule

        // transpose matrix
        final float src0  = mat4[0];
        final float src4  = mat4[1];
        final float src8  = mat4[2];
        final float src12 = mat4[3];

        final float src1  = mat4[4];
        final float src5  = mat4[5];
        final float src9  = mat4[6];
        final float src13 = mat4[7];

        final float src2  = mat4[8];
        final float src6  = mat4[9];
        final float src10 = mat4[10];
        final float src14 = mat4[11];

        final float src3  = mat4[12];
        final float src7  = mat4[13];
        final float src11 = mat4[14];
        final float src15 = mat4[15];

        // calculate pairs for first 8 elements (cofactors)
        final float atmp0 = src10 * src15;
        final float atmp1 = src11 * src14;
        final float atmp2 = src9 * src15;
        final float atmp3 = src11 * src13;
        final float atmp4 = src9 * src14;
        final float atmp5 = src10 * src13;
        final float atmp6 = src8 * src15;
        final float atmp7 = src11 * src12;
        final float atmp8 = src8 * src14;
        final float atmp9 = src10 * src12;
        final float atmp10 = src8 * src13;
        final float atmp11 = src9 * src12;

        // calculate first 8 elements (cofactors)
        final float dst0 = (atmp0 * src5 + atmp3 * src6 + atmp4 * src7)
                - (atmp1 * src5 + atmp2 * src6 + atmp5 * src7);
        final float dst1 = (atmp1 * src4 + atmp6 * src6 + atmp9 * src7)
                - (atmp0 * src4 + atmp7 * src6 + atmp8 * src7);
        final float dst2 = (atmp2 * src4 + atmp7 * src5 + atmp10 * src7)
                - (atmp3 * src4 + atmp6 * src5 + atmp11 * src7);
        final float dst3 = (atmp5 * src4 + atmp8 * src5 + atmp11 * src6)
                - (atmp4 * src4 + atmp9 * src5 + atmp10 * src6);
        final float dst4 = (atmp1 * src1 + atmp2 * src2 + atmp5 * src3)
                - (atmp0 * src1 + atmp3 * src2 + atmp4 * src3);
        final float dst5 = (atmp0 * src0 + atmp7 * src2 + atmp8 * src3)
                - (atmp1 * src0 + atmp6 * src2 + atmp9 * src3);
        final float dst6 = (atmp3 * src0 + atmp6 * src1 + atmp11 * src3)
                - (atmp2 * src0 + atmp7 * src1 + atmp10 * src3);
        final float dst7 = (atmp4 * src0 + atmp9 * src1 + atmp10 * src2)
                - (atmp5 * src0 + atmp8 * src1 + atmp11 * src2);

        // calculate pairs for second 8 elements (cofactors)
        final float btmp0 = src2 * src7;
        final float btmp1 = src3 * src6;
        final float btmp2 = src1 * src7;
        final float btmp3 = src3 * src5;
        final float btmp4 = src1 * src6;
        final float btmp5 = src2 * src5;
        final float btmp6 = src0 * src7;
        final float btmp7 = src3 * src4;
        final float btmp8 = src0 * src6;
        final float btmp9 = src2 * src4;
        final float btmp10 = src0 * src5;
        final float btmp11 = src1 * src4;

        // calculate second 8 elements (cofactors)
        final float dst8 = (btmp0 * src13 + btmp3 * src14 + btmp4 * src15)
                - (btmp1 * src13 + btmp2 * src14 + btmp5 * src15);
        final float dst9 = (btmp1 * src12 + btmp6 * src14 + btmp9 * src15)
                - (btmp0 * src12 + btmp7 * src14 + btmp8 * src15);
        final float dst10 = (btmp2 * src12 + btmp7 * src13 + btmp10 * src15)
                - (btmp3 * src12 + btmp6 * src13 + btmp11 * src15);
        final float dst11 = (btmp5 * src12 + btmp8 * src13 + btmp11 * src14)
                - (btmp4 * src12 + btmp9 * src13 + btmp10 * src14);
        final float dst12 = (btmp2 * src10 + btmp5 * src11 + btmp1 * src9)
                - (btmp4 * src11 + btmp0 * src9 + btmp3 * src10);
        final float dst13 = (btmp8 * src11 + btmp0 * src8 + btmp7 * src10)
                - (btmp6 * src10 + btmp9 * src11 + btmp1 * src8);
        final float dst14 = (btmp6 * src9 + btmp11 * src11 + btmp3 * src8)
                - (btmp10 * src11 + btmp2 * src8 + btmp7 * src9);
        final float dst15 = (btmp10 * src10 + btmp4 * src8 + btmp9 * src9)
                - (btmp8 * src9 + btmp11 * src10 + btmp5 * src8);

        // calculate determinant
        final float det = src0 * dst0 + src1 * dst1 + src2 * dst2 + src3 * dst3;

        if (det == 0) return false;

        // calculate matrix inverse
        final float invdet = 1 / det;
        result_mat4[0] = dst0 * invdet;
        result_mat4[1] = dst1 * invdet;
        result_mat4[2] = dst2 * invdet;
        result_mat4[3] = dst3 * invdet;

        result_mat4[4] = dst4 * invdet;
        result_mat4[5] = dst5 * invdet;
        result_mat4[6] = dst6 * invdet;
        result_mat4[7] = dst7 * invdet;

        result_mat4[8] = dst8 * invdet;
        result_mat4[9] = dst9 * invdet;
        result_mat4[10] = dst10 * invdet;
        result_mat4[11] = dst11 * invdet;

        result_mat4[12] = dst12 * invdet;
        result_mat4[13] = dst13 * invdet;
        result_mat4[14] = dst14 * invdet;
        result_mat4[15] = dst15 * invdet;

        return true;
    }

    public static void transposeM(float[] result_mat4, float[] mat4) {
        arraycopy(mat4, 0, temp, 0, 16);
        result_mat4[ 0] = temp[ 0];
        result_mat4[ 1] = temp[ 4];
        result_mat4[ 2] = temp[ 8];
        result_mat4[ 3] = temp[12];
        result_mat4[ 4] = temp[ 1];
        result_mat4[ 5] = temp[ 5];
        result_mat4[ 6] = temp[ 9];
        result_mat4[ 7] = temp[13];
        result_mat4[ 8] = temp[ 2];
        result_mat4[ 9] = temp[ 6];
        result_mat4[10] = temp[10];
        result_mat4[11] = temp[14];
        result_mat4[12] = temp[ 3];
        result_mat4[13] = temp[ 7];
        result_mat4[14] = temp[11];
        result_mat4[15] = temp[15];
    }

    public static void perspectiveM(float[] result_mat4, float fovy, float aspect, float near, float far) {
        float f = 1 / (float) tan(fovy * (PI / 360));
        float rangeReciprocal = 1 / (near - far);

        result_mat4[0] = f / aspect;
        result_mat4[1] = 0;
        result_mat4[2] = 0;
        result_mat4[3] = 0;

        result_mat4[4] = 0;
        result_mat4[5] = f;
        result_mat4[6] = 0;
        result_mat4[7] = 0;

        result_mat4[8] = 0;
        result_mat4[9] = 0;
        result_mat4[10] = (far + near) * rangeReciprocal;
        result_mat4[11] = -1;

        result_mat4[12] = 0;
        result_mat4[13] = 0;
        result_mat4[14] = 2 * far * near * rangeReciprocal;
        result_mat4[15] = 0;
    }

    public static void orthoM(float[] result_mat4, float left, float right, float bottom, float top, float near, float far) {
        if (left == right)  throw new IllegalArgumentException("left == right");
        if (bottom == top)  throw new IllegalArgumentException("bottom == top");
        if (near == far)    throw new IllegalArgumentException("near == far");

        final float r_width = 1 / (right - left);
        final float r_height = 1 / (top - bottom);
        final float r_depth = 1 / (far - near);

        result_mat4[ 0] = 2 * (r_width);
        result_mat4[ 1] = 0;
        result_mat4[ 2] = 0;
        result_mat4[ 3] = 0;

        result_mat4[ 4] = 0;
        result_mat4[ 5] = 2 * (r_height);
        result_mat4[ 6] = 0;
        result_mat4[ 7] = 0;

        result_mat4[ 8] = 0;
        result_mat4[ 9] = 0;
        result_mat4[10] = -2 * (r_depth);
        result_mat4[11] = 0;

        result_mat4[12] = -(right + left) * r_width;
        result_mat4[13] = -(top + bottom) * r_height;
        result_mat4[14] = -(far + near) * r_depth;
        result_mat4[15] = 1;
    }

    public static void frustumM(float[] result_mat4, float left, float right, float bottom, float top, float near, float far) {
        if (left == right)  throw new IllegalArgumentException("left == right");
        if (top == bottom)  throw new IllegalArgumentException("top == bottom");
        if (near == far)    throw new IllegalArgumentException("near == far");
        if (near <= 0)      throw new IllegalArgumentException("near <= 0.0f");
        if (far <= 0)       throw new IllegalArgumentException("far <= 0.0f");

        final float r_width = 1 / (right - left);
        final float r_height = 1 / (top - bottom);
        final float r_depth = 1 / (near - far);

        result_mat4[ 0] = 2 * (near * r_width);
        result_mat4[ 1] = 0;
        result_mat4[ 2] = 0;
        result_mat4[ 3] = 0;

        result_mat4[ 4] = 0;
        result_mat4[ 5] = 2 * (near * r_height);
        result_mat4[ 6] = 0;
        result_mat4[ 7] = 0;

        result_mat4[ 8] = (right + left) * r_width;
        result_mat4[ 9] = (top + bottom) * r_height;
        result_mat4[10] = (far + near) * r_depth;
        result_mat4[11] = -1;

        result_mat4[12] = 0;
        result_mat4[13] = 0;
        result_mat4[14] = 2 * (far * near * r_depth);
        result_mat4[15] = 0;
    }

    public static void setIdentityM(float[] result_mat4) {
        result_mat4[ 0] = 1;
        result_mat4[ 1] = 0;
        result_mat4[ 2] = 0;
        result_mat4[ 3] = 0;

        result_mat4[ 4] = 0;
        result_mat4[ 5] = 1;
        result_mat4[ 6] = 0;
        result_mat4[ 7] = 0;

        result_mat4[ 8] = 0;
        result_mat4[ 9] = 0;
        result_mat4[10] = 1;
        result_mat4[11] = 0;

        result_mat4[12] = 0;
        result_mat4[13] = 0;
        result_mat4[14] = 0;
        result_mat4[15] = 1;
    }

    public static void scaleM(float[] result_mat4, float[] lhs_mat4, float[] rhs_vec3) {
        arraycopy(lhs_mat4, 0, result_mat4, 0, 16);
        scaleM(result_mat4, rhs_vec3);
    }

    public static void scaleM(float[] result_mat4, float[] vec3) {
        result_mat4[ 0] *= vec3[0];
        result_mat4[ 1] *= vec3[0];
        result_mat4[ 2] *= vec3[0];
        result_mat4[ 3] *= vec3[0];

        result_mat4[ 4] *= vec3[1];
        result_mat4[ 5] *= vec3[1];
        result_mat4[ 6] *= vec3[1];
        result_mat4[ 7] *= vec3[1];

        result_mat4[ 8] *= vec3[2];
        result_mat4[ 9] *= vec3[2];
        result_mat4[10] *= vec3[2];
        result_mat4[11] *= vec3[2];
    }

    public static void translateM(float[] result_mat4, float[] lhs_mat4, float[] rhs_vec3) {
        arraycopy(lhs_mat4, 0, result_mat4, 0, 16);
        translateM(result_mat4, rhs_vec3);
    }

    public static void translateM(float[] result_mat4, float[] vec3) {
        result_mat4[12] += result_mat4[0] * vec3[0] + result_mat4[4] * vec3[1] + result_mat4[ 8] * vec3[2];
        result_mat4[13] += result_mat4[1] * vec3[0] + result_mat4[5] * vec3[1] + result_mat4[ 9] * vec3[2];
        result_mat4[14] += result_mat4[2] * vec3[0] + result_mat4[6] * vec3[1] + result_mat4[10] * vec3[2];
        result_mat4[15] += result_mat4[3] * vec3[0] + result_mat4[7] * vec3[1] + result_mat4[11] * vec3[2];
    }

    public static void rotateM(float[] result_mat4, float[] lhs_mat4, float[] axisAngle_vec4) {
        setRotateM(temp0, axisAngle_vec4);
        multiplyMM(result_mat4, lhs_mat4, temp0);
    }

    public static void rotateM(float[] result_mat4, float[] axisAngle_vec4) {
        setRotateM(temp0, axisAngle_vec4);
        multiplyMM(temp1, result_mat4, temp0);
        arraycopy(temp1, 0, result_mat4, 0, 16);
    }

    public static void rotateMQ(float[] result_mat4, float[] inQuat) {
        Quat.toAxisAngle(TMP0, 0, inQuat, 0, true);
        rotateM(result_mat4, TMP0);
    }

    public static void setRotateM(float[] result_mat4, float[] axisAngle_vec4) {
        float axis_x = axisAngle_vec4[0];
        float axis_y = axisAngle_vec4[1];
        float axis_z = axisAngle_vec4[2];

        float angle = axisAngle_vec4[3] * TO_RADIANS;

        float sin_angle = (float) sin(angle);
        float cos_angle = (float) cos(angle);

        if (axis_x == 1 &&
            axis_y == 0 &&
            axis_z == 0) {

            result_mat4[ 0] = 1;
            result_mat4[ 1] = 0;
            result_mat4[ 2] = 0;
            result_mat4[ 3] = 0;

            result_mat4[ 4] = 0;
            result_mat4[ 5] = cos_angle;
            result_mat4[ 6] = sin_angle;
            result_mat4[ 7] = 0;

            result_mat4[ 8] = 0;
            result_mat4[ 9] = -sin_angle;
            result_mat4[10] = cos_angle;
            result_mat4[11] = 0;

        } else
        if (axis_x == 0 &&
            axis_y == 1 &&
            axis_z == 0) {

            result_mat4[ 0] = cos_angle;
            result_mat4[ 1] = 0;
            result_mat4[ 2] = -sin_angle;
            result_mat4[ 3] = 0;

            result_mat4[ 4] = 0;
            result_mat4[ 5] = 1;
            result_mat4[ 6] = 0;
            result_mat4[ 7] = 0;

            result_mat4[ 8] = sin_angle;
            result_mat4[ 9] = 0;
            result_mat4[10] = cos_angle;
            result_mat4[11] = 0;

        } else
        if (axis_x == 0 &&
            axis_y == 0 &&
            axis_z == 1) {

            result_mat4[ 0] = cos_angle;
            result_mat4[ 1] = sin_angle;
            result_mat4[ 2] = 0;
            result_mat4[ 3] = 0;

            result_mat4[ 4] = -sin_angle;
            result_mat4[ 5] = cos_angle;
            result_mat4[ 6] = 0;
            result_mat4[ 7] = 0;

            result_mat4[ 8] = 0;
            result_mat4[ 9] = 0;
            result_mat4[10] = 1;
            result_mat4[11] = 0;

        } else {
            float len = (float) sqrt(axis_x * axis_x + axis_y * axis_y + axis_z * axis_z);
            if (len != 1) {
                float recipLen = 1 / len;
                axis_x *= recipLen;
                axis_y *= recipLen;
                axis_z *= recipLen;
            }

            float nc = 1 - cos_angle;
            float xy = axis_x * axis_y;
            float yz = axis_y * axis_z;
            float zx = axis_z * axis_x;
            float xs = axis_x * sin_angle;
            float ys = axis_y * sin_angle;
            float zs = axis_z * sin_angle;

            result_mat4[ 0] = axis_x * axis_x * nc + cos_angle;
            result_mat4[ 1] = xy * nc + zs;
            result_mat4[ 2] = zx * nc - ys;
            result_mat4[ 3] = 0;

            result_mat4[ 4] = xy * nc - zs;
            result_mat4[ 5] = axis_y * axis_y * nc + cos_angle;
            result_mat4[ 6] = yz * nc + xs;
            result_mat4[ 7] = 0;

            result_mat4[ 8] = zx * nc + ys;
            result_mat4[ 9] = yz * nc - xs;
            result_mat4[10] = axis_z * axis_z * nc + cos_angle;
            result_mat4[11] = 0;
        }

        result_mat4[12] = 0;
        result_mat4[13] = 0;
        result_mat4[14] = 0;
        result_mat4[15] = 1;
    }

    public static void setRotateEulerM(float[] result_mat4, float x, float y, float z) {
        x *= TO_RADIANS;
        y *= TO_RADIANS;
        z *= TO_RADIANS;

        float cos_x = (float) cos(x);
        float sin_x = (float) sin(x);
        float cos_y = (float) cos(y);
        float sin_y = (float) sin(y);
        float cos_z = (float) cos(z);
        float sin_z = (float) sin(z);

        float cos_x_sin_y = cos_x * sin_y;
        float sin_x_sin_y = sin_x * sin_y;

        result_mat4[ 0] = cos_y * cos_z;
        result_mat4[ 1] = -cos_y * sin_z;
        result_mat4[ 2] = sin_y;
        result_mat4[ 3] = 0;

        result_mat4[ 4] = cos_x_sin_y * cos_z + cos_x * sin_z;
        result_mat4[ 5] = -cos_x_sin_y * sin_z + cos_x * cos_z;
        result_mat4[ 6] = -sin_x * cos_y;
        result_mat4[ 7] = 0;

        result_mat4[ 8] = -sin_x_sin_y * cos_z + sin_x * sin_z;
        result_mat4[ 9] = sin_x_sin_y * sin_z + sin_x * cos_z;
        result_mat4[10] = cos_x * cos_y;
        result_mat4[11] = 0;

        result_mat4[12] = 0;
        result_mat4[13] = 0;
        result_mat4[14] = 0;
        result_mat4[15] = 1;
    }

    public static void setLookAtM(float[] result_mat4, float[] eye_vec3, float[] target_vec3, float[] up_vec3) {

        // See the OpenGL GLUT documentation for gluLookAt for a description
        // of the algorithm. We implement it in a straightforward way:

        float fx = target_vec3[0] - eye_vec3[0];
        float fy = target_vec3[1] - eye_vec3[1];
        float fz = target_vec3[2] - eye_vec3[2];

        // Normalize f
        float rlf = 1 / length(fx, fy, fz);
        fx *= rlf;
        fy *= rlf;
        fz *= rlf;

        // compute s = f x up (x means "cross product")
        float sx = fy * up_vec3[2] - fz * up_vec3[1];
        float sy = fz * up_vec3[0] - fx * up_vec3[2];
        float sz = fx * up_vec3[1] - fy * up_vec3[0];

        // and normalize s
        float rls = 1 / length(sx, sy, sz);
        sx *= rls;
        sy *= rls;
        sz *= rls;

        // compute u = s x f
        float ux = sy * fz - sz * fy;
        float uy = sz * fx - sx * fz;
        float uz = sx * fy - sy * fx;

        result_mat4[ 0] = sx;
        result_mat4[ 1] = ux;
        result_mat4[ 2] = -fx;
        result_mat4[ 3] = 0;

        result_mat4[ 4] = sy;
        result_mat4[ 5] = uy;
        result_mat4[ 6] = -fy;
        result_mat4[ 7] = 0;

        result_mat4[ 8] = sz;
        result_mat4[ 9] = uz;
        result_mat4[10] = -fz;
        result_mat4[11] = 0;

        result_mat4[12] = 0;
        result_mat4[13] = 0;
        result_mat4[14] = 0;
        result_mat4[15] = 1;

        Vec3.mul(temp0, eye_vec3, -1);
        translateM(result_mat4, temp0);
    }

    public static float length(float x, float y, float z) {
        return (float) sqrt(x * x + y * y + z * z);
    }

    @Deprecated
    private final static float[] temp = new float[32];

    @Deprecated
    public static void perspectiveM(float[] m, int offset, float fovy, float aspect, float zNear, float zFar) {
        float f = 1.0f / (float) tan(fovy * (PI / 360.0));
        float rangeReciprocal = 1.0f / (zNear - zFar);

        m[offset + 0] = f / aspect;
        m[offset + 1] = 0.0f;
        m[offset + 2] = 0.0f;
        m[offset + 3] = 0.0f;

        m[offset + 4] = 0.0f;
        m[offset + 5] = f;
        m[offset + 6] = 0.0f;
        m[offset + 7] = 0.0f;

        m[offset + 8] = 0.0f;
        m[offset + 9] = 0.0f;
        m[offset + 10] = (zFar + zNear) * rangeReciprocal;
        m[offset + 11] = -1.0f;

        m[offset + 12] = 0.0f;
        m[offset + 13] = 0.0f;
        m[offset + 14] = 2.0f * zFar * zNear * rangeReciprocal;
        m[offset + 15] = 0.0f;
    }

    @Deprecated
    public static void orthoM(float[] m, int mOffset, float left, float right, float bottom, float top, float near, float far) {
        if (left == right)  throw new IllegalArgumentException("left == right");
        if (bottom == top)  throw new IllegalArgumentException("bottom == top");
        if (near == far)    throw new IllegalArgumentException("near == far");

        final float r_width = 1.0f / (right - left);
        final float r_height = 1.0f / (top - bottom);
        final float r_depth = 1.0f / (far - near);
        final float x = 2.0f * (r_width);
        final float y = 2.0f * (r_height);
        final float z = -2.0f * (r_depth);
        final float tx = -(right + left) * r_width;
        final float ty = -(top + bottom) * r_height;
        final float tz = -(far + near) * r_depth;

        m[mOffset + 0] = x;
        m[mOffset + 5] = y;
        m[mOffset + 10] = z;
        m[mOffset + 12] = tx;
        m[mOffset + 13] = ty;
        m[mOffset + 14] = tz;
        m[mOffset + 15] = 1.0f;
        m[mOffset + 1] = 0.0f;
        m[mOffset + 2] = 0.0f;
        m[mOffset + 3] = 0.0f;
        m[mOffset + 4] = 0.0f;
        m[mOffset + 6] = 0.0f;
        m[mOffset + 7] = 0.0f;
        m[mOffset + 8] = 0.0f;
        m[mOffset + 9] = 0.0f;
        m[mOffset + 11] = 0.0f;
    }

    @Deprecated
    public static void frustumM(float[] m, int offset, float left, float right, float bottom, float top, float near, float far) {
        if (left == right)  throw new IllegalArgumentException("left == right");
        if (top == bottom)  throw new IllegalArgumentException("top == bottom");
        if (near == far)    throw new IllegalArgumentException("near == far");
        if (near <= 0.0f)   throw new IllegalArgumentException("near <= 0.0f");
        if (far <= 0.0f)    throw new IllegalArgumentException("far <= 0.0f");

        final float r_width = 1.0f / (right - left);
        final float r_height = 1.0f / (top - bottom);
        final float r_depth = 1.0f / (near - far);
        final float x = 2.0f * (near * r_width);
        final float y = 2.0f * (near * r_height);
        final float A = (right + left) * r_width;
        final float B = (top + bottom) * r_height;
        final float C = (far + near) * r_depth;
        final float D = 2.0f * (far * near * r_depth);

        m[offset + 0] = x;
        m[offset + 5] = y;
        m[offset + 8] = A;
        m[offset + 9] = B;
        m[offset + 10] = C;
        m[offset + 14] = D;
        m[offset + 11] = -1.0f;
        m[offset + 1] = 0.0f;
        m[offset + 2] = 0.0f;
        m[offset + 3] = 0.0f;
        m[offset + 4] = 0.0f;
        m[offset + 6] = 0.0f;
        m[offset + 7] = 0.0f;
        m[offset + 12] = 0.0f;
        m[offset + 13] = 0.0f;
        m[offset + 15] = 0.0f;
    }

    @Deprecated
    public static void multiplyMM(float[] result, int resultOffset, float[] lhs_ref, int lhsOffset, float[] rhs_ref, int rhsOffset) {
        float[] lhs = new float[16];
        float[] rhs = new float[16];

        float[] resultMat = new float[16];

        arraycopy(lhs_ref, lhsOffset, lhs, 0, 16);
        arraycopy(rhs_ref, rhsOffset, rhs, 0, 16);

        multiplyMM(resultMat, lhs, rhs);

        arraycopy(resultMat, 0, result, resultOffset, 16);
    }

    @Deprecated
    public static void multiplyMV(float[] result_ref, int resultOffset, float[] lhs_ref, int lhsOffset, float[] rhs_ref, int rhsOffset) {
        float[] lhs = new float[16];
        float[] rhs = new float[4];

        float[] resultV = new float[4];

        arraycopy(lhs_ref, lhsOffset, lhs, 0, 16);
        arraycopy(rhs_ref, rhsOffset, rhs, 0, 4);

        multiplyMV(resultV, lhs, rhs);

        arraycopy(resultV, 0, result_ref, resultOffset, 4);
    }

    @Deprecated
    public static boolean invertM(float[] mInv, int mInvOffset, float[] m, int mOffset) {
        // Invert a 4 x 4 matrix using Cramer's Rule

        // transpose matrix
        final float src0 = m[mOffset + 0];
        final float src4 = m[mOffset + 1];
        final float src8 = m[mOffset + 2];
        final float src12 = m[mOffset + 3];

        final float src1 = m[mOffset + 4];
        final float src5 = m[mOffset + 5];
        final float src9 = m[mOffset + 6];
        final float src13 = m[mOffset + 7];

        final float src2 = m[mOffset + 8];
        final float src6 = m[mOffset + 9];
        final float src10 = m[mOffset + 10];
        final float src14 = m[mOffset + 11];

        final float src3 = m[mOffset + 12];
        final float src7 = m[mOffset + 13];
        final float src11 = m[mOffset + 14];
        final float src15 = m[mOffset + 15];

        // calculate pairs for first 8 elements (cofactors)
        final float atmp0 = src10 * src15;
        final float atmp1 = src11 * src14;
        final float atmp2 = src9 * src15;
        final float atmp3 = src11 * src13;
        final float atmp4 = src9 * src14;
        final float atmp5 = src10 * src13;
        final float atmp6 = src8 * src15;
        final float atmp7 = src11 * src12;
        final float atmp8 = src8 * src14;
        final float atmp9 = src10 * src12;
        final float atmp10 = src8 * src13;
        final float atmp11 = src9 * src12;

        // calculate first 8 elements (cofactors)
        final float dst0 = (atmp0 * src5 + atmp3 * src6 + atmp4 * src7)
                - (atmp1 * src5 + atmp2 * src6 + atmp5 * src7);
        final float dst1 = (atmp1 * src4 + atmp6 * src6 + atmp9 * src7)
                - (atmp0 * src4 + atmp7 * src6 + atmp8 * src7);
        final float dst2 = (atmp2 * src4 + atmp7 * src5 + atmp10 * src7)
                - (atmp3 * src4 + atmp6 * src5 + atmp11 * src7);
        final float dst3 = (atmp5 * src4 + atmp8 * src5 + atmp11 * src6)
                - (atmp4 * src4 + atmp9 * src5 + atmp10 * src6);
        final float dst4 = (atmp1 * src1 + atmp2 * src2 + atmp5 * src3)
                - (atmp0 * src1 + atmp3 * src2 + atmp4 * src3);
        final float dst5 = (atmp0 * src0 + atmp7 * src2 + atmp8 * src3)
                - (atmp1 * src0 + atmp6 * src2 + atmp9 * src3);
        final float dst6 = (atmp3 * src0 + atmp6 * src1 + atmp11 * src3)
                - (atmp2 * src0 + atmp7 * src1 + atmp10 * src3);
        final float dst7 = (atmp4 * src0 + atmp9 * src1 + atmp10 * src2)
                - (atmp5 * src0 + atmp8 * src1 + atmp11 * src2);

        // calculate pairs for second 8 elements (cofactors)
        final float btmp0 = src2 * src7;
        final float btmp1 = src3 * src6;
        final float btmp2 = src1 * src7;
        final float btmp3 = src3 * src5;
        final float btmp4 = src1 * src6;
        final float btmp5 = src2 * src5;
        final float btmp6 = src0 * src7;
        final float btmp7 = src3 * src4;
        final float btmp8 = src0 * src6;
        final float btmp9 = src2 * src4;
        final float btmp10 = src0 * src5;
        final float btmp11 = src1 * src4;

        // calculate second 8 elements (cofactors)
        final float dst8 = (btmp0 * src13 + btmp3 * src14 + btmp4 * src15)
                - (btmp1 * src13 + btmp2 * src14 + btmp5 * src15);
        final float dst9 = (btmp1 * src12 + btmp6 * src14 + btmp9 * src15)
                - (btmp0 * src12 + btmp7 * src14 + btmp8 * src15);
        final float dst10 = (btmp2 * src12 + btmp7 * src13 + btmp10 * src15)
                - (btmp3 * src12 + btmp6 * src13 + btmp11 * src15);
        final float dst11 = (btmp5 * src12 + btmp8 * src13 + btmp11 * src14)
                - (btmp4 * src12 + btmp9 * src13 + btmp10 * src14);
        final float dst12 = (btmp2 * src10 + btmp5 * src11 + btmp1 * src9)
                - (btmp4 * src11 + btmp0 * src9 + btmp3 * src10);
        final float dst13 = (btmp8 * src11 + btmp0 * src8 + btmp7 * src10)
                - (btmp6 * src10 + btmp9 * src11 + btmp1 * src8);
        final float dst14 = (btmp6 * src9 + btmp11 * src11 + btmp3 * src8)
                - (btmp10 * src11 + btmp2 * src8 + btmp7 * src9);
        final float dst15 = (btmp10 * src10 + btmp4 * src8 + btmp9 * src9)
                - (btmp8 * src9 + btmp11 * src10 + btmp5 * src8);

        // calculate determinant
        final float det = src0 * dst0 + src1 * dst1 + src2 * dst2 + src3 * dst3;

        if (det == 0.0f) {
            return false;
        }

        // calculate matrix inverse
        final float invdet = 1.0f / det;
        mInv[mInvOffset] = dst0 * invdet;
        mInv[1 + mInvOffset] = dst1 * invdet;
        mInv[2 + mInvOffset] = dst2 * invdet;
        mInv[3 + mInvOffset] = dst3 * invdet;

        mInv[4 + mInvOffset] = dst4 * invdet;
        mInv[5 + mInvOffset] = dst5 * invdet;
        mInv[6 + mInvOffset] = dst6 * invdet;
        mInv[7 + mInvOffset] = dst7 * invdet;

        mInv[8 + mInvOffset] = dst8 * invdet;
        mInv[9 + mInvOffset] = dst9 * invdet;
        mInv[10 + mInvOffset] = dst10 * invdet;
        mInv[11 + mInvOffset] = dst11 * invdet;

        mInv[12 + mInvOffset] = dst12 * invdet;
        mInv[13 + mInvOffset] = dst13 * invdet;
        mInv[14 + mInvOffset] = dst14 * invdet;
        mInv[15 + mInvOffset] = dst15 * invdet;

        return true;
    }

    @Deprecated
    public static void setIdentityM(float[] sm, int smOffset) {
        for (int i = 0; i < 16; i++) {
            sm[smOffset + i] = 0;
        }
        for (int i = 0; i < 16; i += 5) {
            sm[smOffset + i] = 1.0f;
        }
    }

    @Deprecated
    public static void transposeM(float[] mTrans, int mTransOffset, float[] m, int mOffset) {
        for (int i = 0; i < 4; i++) {
            int mBase = i * 4 + mOffset;
            mTrans[i + mTransOffset] = m[mBase];
            mTrans[i + 4 + mTransOffset] = m[mBase + 1];
            mTrans[i + 8 + mTransOffset] = m[mBase + 2];
            mTrans[i + 12 + mTransOffset] = m[mBase + 3];
        }
    }

    @Deprecated
    public static void scaleM(float[] sm, int smOffset, float[] m, int mOffset, float x, float y, float z) {
        for (int i = 0; i < 4; i++) {
            int smi = smOffset + i;
            int mi = mOffset + i;
            sm[smi] = m[mi] * x;
            sm[4 + smi] = m[4 + mi] * y;
            sm[8 + smi] = m[8 + mi] * z;
            sm[12 + smi] = m[12 + mi];
        }
    }

    @Deprecated
    public static void scaleM(float[] m, int mOffset, float x, float y, float z) {
        for (int i = 0; i < 4; i++) {
            int mi = mOffset + i;
            m[mi] *= x;
            m[4 + mi] *= y;
            m[8 + mi] *= z;
        }
    }

    @Deprecated
    public static void translateM(float[] tm, int tmOffset, float[] m, int mOffset, float x, float y, float z) {
        for (int i = 0; i < 12; i++) {
            tm[tmOffset + i] = m[mOffset + i];
        }
        for (int i = 0; i < 4; i++) {
            int tmi = tmOffset + i;
            int mi = mOffset + i;
            tm[12 + tmi] = m[mi] * x + m[4 + mi] * y + m[8 + mi] * z + m[12 + mi];
        }
    }

    @Deprecated
    public static void translateM(float[] m, int mOffset, float x, float y, float z) {
        for (int i = 0; i < 4; i++) {
            int mi = mOffset + i;
            m[12 + mi] += m[mi] * x + m[4 + mi] * y + m[8 + mi] * z;
        }
    }

    @Deprecated
    public static void rotateM(float[] rm, int rmOffset, float[] m, int mOffset, float a, float x, float y, float z) {
        synchronized (temp) {
            setRotateM(temp, 0, a, x, y, z);
            multiplyMM(rm, rmOffset, m, mOffset, temp, 0);
        }
    }

    @Deprecated
    public static void rotateM(float[] m, int mOffset, float a, float x, float y, float z) {
        synchronized (temp) {
            setRotateM(temp, 0, a, x, y, z);
            multiplyMM(temp, 16, m, mOffset, temp, 0);
            arraycopy(temp, 16, m, mOffset, 16);
        }
    }

    @Deprecated
    public static void setRotateM(float[] rm, int rmOffset, float a, float x, float y, float z) {
        rm[rmOffset + 3] = 0;
        rm[rmOffset + 7] = 0;
        rm[rmOffset + 11] = 0;
        rm[rmOffset + 12] = 0;
        rm[rmOffset + 13] = 0;
        rm[rmOffset + 14] = 0;
        rm[rmOffset + 15] = 1;
        a *= (float) (PI / 180.0f);
        float s = (float) sin(a);
        float c = (float) cos(a);
        if (1.0f == x && 0.0f == y && 0.0f == z) {
            rm[rmOffset + 5] = c;
            rm[rmOffset + 10] = c;
            rm[rmOffset + 6] = s;
            rm[rmOffset + 9] = -s;
            rm[rmOffset + 1] = 0;
            rm[rmOffset + 2] = 0;
            rm[rmOffset + 4] = 0;
            rm[rmOffset + 8] = 0;
            rm[rmOffset + 0] = 1;
        } else if (0.0f == x && 1.0f == y && 0.0f == z) {
            rm[rmOffset + 0] = c;
            rm[rmOffset + 10] = c;
            rm[rmOffset + 8] = s;
            rm[rmOffset + 2] = -s;
            rm[rmOffset + 1] = 0;
            rm[rmOffset + 4] = 0;
            rm[rmOffset + 6] = 0;
            rm[rmOffset + 9] = 0;
            rm[rmOffset + 5] = 1;
        } else if (0.0f == x && 0.0f == y && 1.0f == z) {
            rm[rmOffset + 0] = c;
            rm[rmOffset + 5] = c;
            rm[rmOffset + 1] = s;
            rm[rmOffset + 4] = -s;
            rm[rmOffset + 2] = 0;
            rm[rmOffset + 6] = 0;
            rm[rmOffset + 8] = 0;
            rm[rmOffset + 9] = 0;
            rm[rmOffset + 10] = 1;
        } else {
            float len = length(x, y, z);
            if (1.0f != len) {
                float recipLen = 1.0f / len;
                x *= recipLen;
                y *= recipLen;
                z *= recipLen;
            }
            float nc = 1.0f - c;
            float xy = x * y;
            float yz = y * z;
            float zx = z * x;
            float xs = x * s;
            float ys = y * s;
            float zs = z * s;
            rm[rmOffset + 0] = x * x * nc + c;
            rm[rmOffset + 4] = xy * nc - zs;
            rm[rmOffset + 8] = zx * nc + ys;
            rm[rmOffset + 1] = xy * nc + zs;
            rm[rmOffset + 5] = y * y * nc + c;
            rm[rmOffset + 9] = yz * nc - xs;
            rm[rmOffset + 2] = zx * nc - ys;
            rm[rmOffset + 6] = yz * nc + xs;
            rm[rmOffset + 10] = z * z * nc + c;
        }
    }

    @Deprecated
    public static void setRotateEulerM(float[] rm, int rmOffset, float x, float y, float z) {
        x *= TO_RADIANS;
        y *= TO_RADIANS;
        z *= TO_RADIANS;

        float cx = (float) cos(x);
        float sx = (float) sin(x);
        float cy = (float) cos(y);
        float sy = (float) sin(y);
        float cz = (float) cos(z);
        float sz = (float) sin(z);
        float cxsy = cx * sy;
        float sxsy = sx * sy;

        rm[rmOffset + 0] = cy * cz;
        rm[rmOffset + 1] = -cy * sz;
        rm[rmOffset + 2] = sy;
        rm[rmOffset + 3] = 0.0f;

        rm[rmOffset + 4] = cxsy * cz + cx * sz;
        rm[rmOffset + 5] = -cxsy * sz + cx * cz;
        rm[rmOffset + 6] = -sx * cy;
        rm[rmOffset + 7] = 0.0f;

        rm[rmOffset + 8] = -sxsy * cz + sx * sz;
        rm[rmOffset + 9] = sxsy * sz + sx * cz;
        rm[rmOffset + 10] = cx * cy;
        rm[rmOffset + 11] = 0.0f;

        rm[rmOffset + 12] = 0.0f;
        rm[rmOffset + 13] = 0.0f;
        rm[rmOffset + 14] = 0.0f;
        rm[rmOffset + 15] = 1.0f;
    }

    @Deprecated
    public static void setLookAtM(float[] rm, int rmOffset, float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ) {

        // See the OpenGL GLUT documentation for gluLookAt for a description
        // of the algorithm. We implement it in a straightforward way:

        float fx = centerX - eyeX;
        float fy = centerY - eyeY;
        float fz = centerZ - eyeZ;

        // Normalize f
        float rlf = 1.0f / length(fx, fy, fz);
        fx *= rlf;
        fy *= rlf;
        fz *= rlf;

        // compute s = f x up (x means "cross product")
        float sx = fy * upZ - fz * upY;
        float sy = fz * upX - fx * upZ;
        float sz = fx * upY - fy * upX;

        // and normalize s
        float rls = 1.0f / length(sx, sy, sz);
        sx *= rls;
        sy *= rls;
        sz *= rls;

        // compute u = s x f
        float ux = sy * fz - sz * fy;
        float uy = sz * fx - sx * fz;
        float uz = sx * fy - sy * fx;

        rm[rmOffset + 0] = sx;
        rm[rmOffset + 1] = ux;
        rm[rmOffset + 2] = -fx;
        rm[rmOffset + 3] = 0.0f;

        rm[rmOffset + 4] = sy;
        rm[rmOffset + 5] = uy;
        rm[rmOffset + 6] = -fy;
        rm[rmOffset + 7] = 0.0f;

        rm[rmOffset + 8] = sz;
        rm[rmOffset + 9] = uz;
        rm[rmOffset + 10] = -fz;
        rm[rmOffset + 11] = 0.0f;

        rm[rmOffset + 12] = 0.0f;
        rm[rmOffset + 13] = 0.0f;
        rm[rmOffset + 14] = 0.0f;
        rm[rmOffset + 15] = 1.0f;

        translateM(rm, rmOffset, -eyeX, -eyeY, -eyeZ);
    }

    @Deprecated
    public static void setLookAtM(float[] rm, final int rmOffset, final float[] eye, final float[] target, final float[] up) {
        setLookAtM(rm, rmOffset, eye[0], eye[1], eye[2], target[0], target[1], target[2], up[0], up[1], up[2]);
    }

    @Deprecated
    public static void translateM(float[] m, final int mOffset, final float[] position) {
        translateM(m, mOffset, position[0], position[1], position[2]);
    }

    @Deprecated
    public static void rotateM(float[] m, final int mOffset, final float a, final float[] axis) {
        rotateM(m, mOffset, a, axis[0], axis[1], axis[2]);
    }

    @Deprecated
    public static void rotateM(float[] m, final int mOffset, final float[] inQuat) {
        Quat.toAxisAngle(TMP0, 0,inQuat, 0, true);
        rotateM(m, mOffset, TMP0[3], TMP0);
    }

    @Deprecated
    public static void scaleM(float[] m, final int mOffset, final float[] scale) {
        scaleM(m, mOffset, scale[0], scale[1], scale[2]);
    }
}