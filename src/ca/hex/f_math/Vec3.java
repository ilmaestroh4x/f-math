package ca.hex.f_math;

import static java.lang.Math.sqrt;
import static java.lang.System.arraycopy;

public final class Vec3 {
    private Vec3() { }

    private final static float[] temp = new float[5];

    private static int rhs_X = 0, lhs_X = 0, v_X = 0;
    private static int rhs_Y = 0, lhs_Y = 0, v_Y = 0;
    private static int rhs_Z = 0, lhs_Z = 0, v_Z = 0;

    public static boolean equals(
            float[] lhs,
            float[] rhs
    ) {
        return
            lhs[0] == rhs[0] &&
            lhs[1] == rhs[1] &&
            lhs[2] == rhs[2];
    }

    public static boolean equals(
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        return
            lhs[lhsOffset    ] == rhs[rhsOffset    ] &&
            lhs[lhsOffset + 1] == rhs[rhsOffset + 1] &&
            lhs[lhsOffset + 2] == rhs[rhsOffset + 2];
    }

    public static void copy(
            float[] result,
            float[] lhs
    ) {
        arraycopy(lhs, 0, result, 0, 3);
    }

    public static void copy(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset
    ) {
        arraycopy(lhs, lhsOffset, result, resultOffset, 3);
    }

    public static float[] zero(
            float[] result
    ) {
        result[0] = 0;
        result[1] = 0;
        result[2] = 0;

        return result;
    }

    public static void zero(
            float[] result, int resultOffset
    ) {
        result[resultOffset    ] = 0;
        result[resultOffset + 1] = 0;
        result[resultOffset + 2] = 0;
    }

    public static float[] one(
            float[] result
    ) {
        result[0] = 1;
        result[1] = 1;
        result[2] = 1;

        return result;
    }

    public static void one(
            float[] result, int resultOffset
    ) {
        result[resultOffset    ] = 1;
        result[resultOffset + 1] = 1;
        result[resultOffset + 2] = 1;
    }

    public static float[] up(
            float[] result
    ) {
        result[0] = 0;
        result[1] = 1;
        result[2] = 0;

        return result;
    }

    public static void up(
            float[] result, int resultOffset
    ) {
        result[resultOffset    ] = 0;
        result[resultOffset + 1] = 1;
        result[resultOffset + 2] = 0;
    }

    public static float[] down(
            float[] result
    ) {
        result[0] =  0;
        result[1] = -1;
        result[2] =  0;

        return result;
    }

    public static void down(
            float[] result, int resultOffset
    ) {
        result[resultOffset    ] =  0;
        result[resultOffset + 1] = -1;
        result[resultOffset + 2] =  0;
    }

    public static float[] right(
            float[] result
    ) {
        result[0] = 1;
        result[1] = 0;
        result[2] = 0;

        return result;
    }

    public static void right(
            float[] result, int resultOffset
    ) {
        result[resultOffset    ] = 1;
        result[resultOffset + 1] = 0;
        result[resultOffset + 2] = 0;
    }

    public static float[] left(
            float[] result
    ) {
        result[0] = -1;
        result[1] =  0;
        result[2] =  0;

        return result;
    }

    public static void left(
            float[] result, int resultOffset
    ) {
        result[resultOffset    ] = -1;
        result[resultOffset + 1] =  0;
        result[resultOffset + 2] =  0;
    }

    public static float[] forward(
            float[] result
    ) {
        result[0] = 0;
        result[1] = 0;
        result[2] = 1;

        return result;
    }

    public static void forward(
            float[] result, int resultOffset
    ) {
        result[resultOffset    ] = 0;
        result[resultOffset + 1] = 0;
        result[resultOffset + 2] = 1;
    }

    public static float[] backward(
            float[] result
    ) {
        result[0] =  0;
        result[1] =  0;
        result[2] = -1;

        return result;
    }

    public static void backward(
            float[] result, int resultOffset
    ) {
        result[resultOffset    ] =  0;
        result[resultOffset + 1] =  0;
        result[resultOffset + 2] = -1;
    }

    public static float[] add(
            float[] result,
            float[] lhs,
            float[] rhs
    ) {
        result[0] = lhs[0] + rhs[0];
        result[1] = lhs[1] + rhs[1];
        result[2] = lhs[2] + rhs[2];

        return result;
    }

    public static void add(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        result[resultOffset    ] = lhs[lhsOffset    ] + rhs[rhsOffset    ];
        result[resultOffset + 1] = lhs[lhsOffset + 1] + rhs[rhsOffset + 1];
        result[resultOffset + 2] = lhs[lhsOffset + 2] + rhs[rhsOffset + 2];
    }

    public static float[] sub(
            float[] result,
            float[] lhs,
            float[] rhs
    ) {
        result[0] = lhs[0] - rhs[0];
        result[1] = lhs[1] - rhs[1];
        result[2] = lhs[2] - rhs[2];

        return result;
    }

    public static void sub(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        result[resultOffset    ] = lhs[lhsOffset    ] - rhs[rhsOffset    ];
        result[resultOffset + 1] = lhs[lhsOffset + 1] - rhs[rhsOffset + 1];
        result[resultOffset + 2] = lhs[lhsOffset + 2] - rhs[rhsOffset + 2];
    }

    public static float[] mul(
            float[] result,
            float[] v,
            float scalar
    ) {
        result[0] = v[0] * scalar;
        result[1] = v[1] * scalar;
        result[2] = v[2] * scalar;

        return result;
    }

    public static void mul(
            float[] result, int resultOffset,
            float[] v,      int vOffset,
            float scalar
    ) {
        result[resultOffset    ] = v[vOffset    ] * scalar;
        result[resultOffset + 1] = v[vOffset + 1] * scalar;
        result[resultOffset + 2] = v[vOffset + 2] * scalar;
    }

    public static float[] div(
            float[] result,
            float[] v,
            float scalar
    ) {
        result[0] = v[0] / scalar;
        result[1] = v[1] / scalar;
        result[2] = v[2] / scalar;

        return result;
    }

    public static void div(
            float[] result, int resultOffset,
            float[] v,      int vOffset,
            float scalar
    ) {
        result[resultOffset    ] = v[vOffset    ] / scalar;
        result[resultOffset + 1] = v[vOffset + 1] / scalar;
        result[resultOffset + 2] = v[vOffset + 2] / scalar;
    }

    public static float dot(
            float[] result,
            float[] lhs,
            float[] rhs
    ) {
        temp[0] = lhs[0] * rhs[0];
        temp[1] = lhs[1] * rhs[1];
        temp[2] = lhs[2] * rhs[2];

        return result[0] = temp[0] + temp[1] + temp[2];
    }

    public static void dot(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        temp[0] = lhs[lhsOffset    ] * rhs[rhsOffset    ];
        temp[1] = lhs[lhsOffset + 1] * rhs[rhsOffset + 1];
        temp[2] = lhs[lhsOffset + 2] * rhs[rhsOffset + 2];

        result[resultOffset] = temp[0] + temp[1] + temp[2];
    }

    public static float[] cross(
            float[] result,
            float[] lhs,
            float[] rhs
    ) {
        temp[0] = lhs[1] * rhs[2] - lhs[2] * rhs[1];
        temp[1] = lhs[2] * rhs[0] - lhs[0] * rhs[2];
        temp[2] = lhs[0] * rhs[1] - lhs[1] * rhs[0];

        result[0] = temp[0];
        result[1] = temp[1];
        result[2] = temp[2];

        return result;
    }

    public static void cross(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        rhs_X = rhsOffset;
        rhs_Y = rhsOffset + 1;
        rhs_Z = rhsOffset + 2;

        lhs_X = lhsOffset;
        lhs_Y = lhsOffset + 1;
        lhs_Z = lhsOffset + 2;

        temp[0] = lhs[lhs_Y] * rhs[rhs_Z] - lhs[lhs_Z] * rhs[rhs_Y];
        temp[1] = lhs[lhs_Z] * rhs[rhs_X] - lhs[lhs_X] * rhs[rhs_Z];
        temp[2] = lhs[lhs_X] * rhs[rhs_Y] - lhs[lhs_Y] * rhs[rhs_X];

        result[resultOffset    ] = temp[0];
        result[resultOffset + 1] = temp[1];
        result[resultOffset + 2] = temp[2];
    }

    public static float[] pow(
            float[] result,
            float[] v,
            float power
    ) {
        result[0] = (float)Math.pow(v[0], power);
        result[1] = (float)Math.pow(v[1], power);
        result[2] = (float)Math.pow(v[2], power);

        return result;
    }

    public static void pow(
            float[] result, int resultOffset,
            float[] v,      int vOffset,
            float power
    ) {
        result[resultOffset    ] = (float)Math.pow(v[vOffset    ], power);
        result[resultOffset + 1] = (float)Math.pow(v[vOffset + 1], power);
        result[resultOffset + 2] = (float)Math.pow(v[vOffset + 2], power);
    }

    public static float[] abs(
            float[] result,
            float[] v
    ) {
        result[0] = Math.abs(v[0]);
        result[1] = Math.abs(v[1]);
        result[2] = Math.abs(v[2]);

        return result;
    }

    public static void abs(
            float[] result, int resultOffset,
            float[] v,      int vOffset
    ) {
        result[resultOffset    ] = Math.abs(v[vOffset    ]);
        result[resultOffset + 1] = Math.abs(v[vOffset + 1]);
        result[resultOffset + 2] = Math.abs(v[vOffset + 2]);
    }

    public static float[] norm(
            float[] result,
            float[] v
    ) {
        temp[0] = v[0] * v[0];
        temp[1] = v[1] * v[1];
        temp[2] = v[2] * v[2];
        temp[3] = (float) sqrt(temp[0] + temp[1] + temp[2]);

        result[0] = v[0] / temp[3];
        result[1] = v[1] / temp[3];
        result[2] = v[2] / temp[3];

        return result;
    }

    public static void norm(
            float[] result, int resultOffset,
            float[] v,      int vOffset
    ) {
        v_X = vOffset;
        v_Y = vOffset + 1;
        v_Z = vOffset + 2;

        temp[0] = v[v_X] * v[v_X];
        temp[1] = v[v_Y] * v[v_Y];
        temp[2] = v[v_Z] * v[v_Z];
        temp[3] = (float) sqrt(temp[0] + temp[1] + temp[2]);

        result[resultOffset    ] = v[v_X] / temp[3];
        result[resultOffset + 1] = v[v_Y] / temp[3];
        result[resultOffset + 2] = v[v_Z] / temp[3];
    }

    public static float mag(
            float[] result,
            float[] v
    ) {
        temp[0] = v[0] * v[0];
        temp[1] = v[1] * v[1];
        temp[2] = v[2] * v[2];

        return result[0] = (float) sqrt(temp[0] + temp[1] + temp[2]);
    }

    public static void mag(
            float[] result, int resultOffset,
            float[] v,      int vOffset
    ) {
        temp[0] = v[vOffset    ];
        temp[1] = v[vOffset + 1];
        temp[2] = v[vOffset + 2];
        temp[0] *= temp[0];
        temp[1] *= temp[1];
        temp[2] *= temp[2];

        result[resultOffset] = (float) sqrt(temp[0] + temp[1] + temp[2]);
    }

    public static float dist(
            float[] result,
            float[] lhs,
            float[] rhs
    ) {
        temp[0] = rhs[0] - lhs[0];
        temp[1] = rhs[1] - lhs[1];
        temp[2] = rhs[2] - lhs[2];

        temp[0] *= temp[0];
        temp[1] *= temp[1];
        temp[2] *= temp[2];

        return result[0] = (float) sqrt(temp[0] + temp[1] + temp[2]);
    }

    public static void dist(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        temp[0] = rhs[rhsOffset    ] - lhs[lhsOffset    ];
        temp[1] = rhs[rhsOffset + 1] - lhs[lhsOffset + 1];
        temp[2] = rhs[rhsOffset + 2] - lhs[lhsOffset + 2];

        temp[0] *= temp[0];
        temp[1] *= temp[1];
        temp[2] *= temp[2];

        result[resultOffset] = (float) sqrt(temp[0] + temp[1] + temp[2]);
    }

    public static float[] reflect(
            float[] result,
            float[] lhs,
            float[] rhs
    ) {
        temp[0] = lhs[0] * rhs[0];
        temp[1] = lhs[1] * rhs[1];
        temp[2] = lhs[2] * rhs[2];

        temp[3] = temp[0] + temp[1] + temp[2] * 2;

        result[0] = lhs[0] - rhs[0] * temp[3];
        result[1] = lhs[1] - rhs[1] * temp[3];
        result[2] = lhs[2] - rhs[2] * temp[3];

        return result;
    }

    public static void reflect(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset
    ) {
        rhs_X = rhsOffset;
        rhs_Y = rhsOffset + 1;
        rhs_Z = rhsOffset + 2;

        lhs_X = lhsOffset;
        lhs_Y = lhsOffset + 1;
        lhs_Z = lhsOffset + 2;

        temp[0] = lhs[lhs_X] * rhs[rhs_X];
        temp[1] = lhs[lhs_Y] * rhs[rhs_Y];
        temp[2] = lhs[lhs_Z] * rhs[rhs_Z];
        temp[3] = temp[0] + temp[1] + temp[2] * 2;

        result[resultOffset    ] = lhs[lhs_X] - rhs[rhs_X] * temp[3];
        result[resultOffset + 1] = lhs[lhs_Y] - rhs[rhs_Y] * temp[3];
        result[resultOffset + 2] = lhs[lhs_Z] - rhs[rhs_Z] * temp[3];
    }

    public static float[] refract(
            float[] result,
            float[] lhs,
            float[] rhs,
            float eta
    ) {
        temp[0] = lhs[0] * rhs[0];
        temp[1] = lhs[1] * rhs[1];
        temp[2] = lhs[2] * rhs[2];

        temp[3] = temp[0] + temp[1] + temp[2];

        temp[4] = 1 - eta * eta * (1 - temp[3] * temp[3]);

        if (temp[4] < 0) {
            result[0] = 0;
            result[1] = 0;
            result[2] = 0;
        } else {
            temp[3] = eta * temp[3] + (float) sqrt(temp[4]);

            result[0] = rhs[0] * eta - lhs[0] * temp[3];
            result[1] = rhs[1] * eta - lhs[1] * temp[3];
            result[2] = rhs[2] * eta - lhs[2] * temp[3];
        }

        return result;
    }

    public static void refract(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset,
            float eta
    ) {
        rhs_X = rhsOffset;
        rhs_Y = rhsOffset + 1;
        rhs_Z = rhsOffset + 2;

        lhs_X = lhsOffset;
        lhs_Y = lhsOffset + 1;
        lhs_Z = lhsOffset + 2;

        temp[0] = lhs[lhs_X] * rhs[rhs_X];
        temp[1] = lhs[lhs_Y] * rhs[rhs_Y];
        temp[2] = lhs[lhs_Z] * rhs[rhs_Z];
        temp[3] = temp[0] + temp[1] + temp[2];
        temp[4] = 1 - eta * eta * (1 - temp[3] * temp[3]);

        if (temp[4] < 0) {
            result[resultOffset    ] = 0;
            result[resultOffset + 1] = 0;
            result[resultOffset + 2] = 0;
        } else {
            temp[3] = eta * temp[3] + (float) sqrt(temp[4]);

            result[resultOffset    ] = rhs[rhs_X] * eta - lhs[lhs_X] * temp[3];
            result[resultOffset + 1] = rhs[rhs_Y] * eta - lhs[lhs_Y] * temp[3];
            result[resultOffset + 2] = rhs[rhs_Z] * eta - lhs[lhs_Z] * temp[3];
        }
    }

    public static float[] step(
            float[] result,
            float[] lhs,
            float[] rhs,
            float dist
    ) {
        temp[0] = rhs[0] - lhs[0];
        temp[1] = rhs[1] - lhs[1];
        temp[2] = rhs[2] - lhs[2];

        temp[0] *= temp[0];
        temp[1] *= temp[1];
        temp[2] *= temp[2];

        temp[3] = dist / (float) sqrt(temp[0] + temp[1] + temp[2]);

        result[0] = lhs[0] + temp[0] * temp[3];
        result[1] = lhs[1] + temp[1] * temp[3];
        result[2] = lhs[2] + temp[2] * temp[3];

        return result;
    }

    public static void step(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset,
            float dist
    ) {
        lhs_X = lhsOffset;
        lhs_Y = lhsOffset + 1;
        lhs_Z = lhsOffset + 2;

        temp[0] = rhs[rhsOffset    ] - lhs[lhs_X];
        temp[1] = rhs[rhsOffset + 1] - lhs[lhs_Y];
        temp[2] = rhs[rhsOffset + 2] - lhs[lhs_Z];
        temp[3] = dist / (float) sqrt(temp[0] + temp[1] + temp[2]);

        result[resultOffset    ] = lhs[lhs_X] + temp[0] * temp[3];
        result[resultOffset + 1] = lhs[lhs_Y] + temp[1] * temp[3];
        result[resultOffset + 2] = lhs[lhs_Z] + temp[2] * temp[3];
    }

    public static float[] lerp(
            float[] result,
            float[] lhs,
            float[] rhs,
            float t
    ) {
        result[0] = lhs[0] + (rhs[0] - lhs[0]) * t;
        result[1] = lhs[1] + (rhs[1] - lhs[1]) * t;
        result[2] = lhs[2] + (rhs[2] - lhs[2]) * t;

        return result;
    }

    public static void lerp(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset,
            float t
    ) {
        lhs_X = lhsOffset;
        lhs_Y = lhsOffset + 1;
        lhs_Z = lhsOffset + 2;

        result[resultOffset    ] = lhs[lhs_X] + (rhs[rhsOffset    ] - lhs[lhs_X]) * t;
        result[resultOffset + 1] = lhs[lhs_Y] + (rhs[rhsOffset + 1] - lhs[lhs_Y]) * t;
        result[resultOffset + 2] = lhs[lhs_Z] + (rhs[rhsOffset + 2] - lhs[lhs_Z]) * t;
    }

    public static float[] nlerp(
            float[] result,
            float[] lhs,
            float[] rhs,
            float t
    ) {
        lerp(result, lhs, rhs, t);
        norm(result, result);

        return result;
    }

    public static void nlerp(
            float[] result, int resultOffset,
            float[] lhs,    int lhsOffset,
            float[] rhs,    int rhsOffset,
            float t
    ) {
        lerp(result, resultOffset, lhs, lhsOffset, rhs, rhsOffset, t);
        norm(result, resultOffset, result, resultOffset);
    }
}